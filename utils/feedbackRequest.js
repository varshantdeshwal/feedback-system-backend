const FeedbackRequest = require("../models/feedbackRequest");
module.exports.add = function(query, callback) {
  FeedbackRequest.create(query, callback);
};
module.exports.fetch = function(query, callback) {
  FeedbackRequest.find(query, callback);
};
module.exports.delete = function(query, callback) {
  FeedbackRequest.remove(query, callback);
};
module.exports.check = function(query, callback) {
  FeedbackRequest.find(query, callback);
};
