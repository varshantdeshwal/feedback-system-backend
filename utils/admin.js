const Admin = require("../models/admin");

module.exports.fetch = function(query, callback) {
  Admin.find(query, callback);
};
module.exports.add = function(query, callback) {
  Admin.create(query, callback);
};
module.exports.fetchName = function(query, callback) {
  Admin.find(query, callback);
};
module.exports.delete = function(query, callback) {
  Admin.remove(query, callback);
};
