const User = require("../models/user");
module.exports.add = function(query, callback) {
  User.create(query, callback);
};
module.exports.check = function(query, callback) {
  User.find(query, "", callback);
};
module.exports.pendingRequests = function(query, callback) {
  User.find(query, callback);
};
module.exports.activeInterviewers = function(query, id, callback) {
  User.find(query, callback)
    .where("_id")
    .ne(id);
};
module.exports.approveRequest = function(id, update, options, callback) {
  var query = { _id: id };

  User.findOneAndUpdate(query, update, options, callback);
};
module.exports.declineRequest = function(query, callback) {
  User.remove(query, callback);
};
module.exports.deleteUser = function(query, callback) {
  User.remove(query, callback);
};
module.exports.getName = function(query, callback) {
  User.find(query, callback);
};
module.exports.edit = function(id, update, options, callback) {
  let query = { _id: id };
  User.findOneAndUpdate(query, update, options, callback);
};
