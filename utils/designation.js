const Designation = require("../models/designation");

module.exports.fetch = function(query, callback) {
  Designation.find(query, callback);
};
module.exports.add = function(query, callback) {
  Designation.create(query, callback);
};

module.exports.delete = function(query, callback) {
  Designation.remove(query, callback);
};
module.exports.check = function(query, callback) {
  Designation.find(query, callback);
};
