const express = require("express");
const app = express();
const bodyParser = require("body-parser");
const mongoose = require("mongoose");

const feedbackRoutes = require("./routes/feedback");
const userRoutes = require("./routes/user");
const adminRoutes = require("./routes/admin");
const skillRoutes = require("./routes/skill");
const designationRoutes = require("./routes/designation");
const feedbackRequestRoutes = require("./routes/feedbackRequest");
app.use(bodyParser.json());
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE");
  res.header(
    "Access-Control-Allow-Headers",
    "Authorization,Accept,Content-Type"
  );

  next();
});
mongoose.connect(
  "mongodb://varshant:nitvasu14@ds123151.mlab.com:23151/feedback-system"
);
var db = mongoose.connection;
db.on("error", console.error.bind(console, "MongoDB connection error:"));

app.use("/feedbacks", feedbackRoutes);
app.use("/users", userRoutes);
app.use("/admins", adminRoutes);
app.use("/skills", skillRoutes);
app.use("/designations", designationRoutes);
app.use("/feedback-requests", feedbackRequestRoutes);
const port = process.env.PORT || 5000;

app.listen(port, () => console.log(`listening at ${port}`));
