const express = require("express");
const router = express.Router();
const FeedbackRequest = require("../utils/feedbackRequest");
const checkAuth = require("../auth/check-auth");
router.post("/", checkAuth, (req, res) => {
  FeedbackRequest.add(req.body, (err, doc) => {
    if (err) {
      res.status(500).json({
        error: err
      });
    } else {
      res.json({ doc: doc, message: "feedback request sent" });
    }
  });
});
router.get("/:id", checkAuth, (req, res, next) => {
  FeedbackRequest.fetch({ newInterviewerId: req.params.id }, (err, doc) => {
    if (err) {
      res.status(500).json({
        error: err
      });
    }
    res.status(200).json({ doc: doc, message: "successful" });
  });
});
router.delete("/:_id", checkAuth, (req, res, next) => {
  FeedbackRequest.delete({ _id: req.params._id }, (err, doc) => {
    if (err) {
      throw err;
    }
    res.json({ message: "deleted", doc: doc });
  });
});
router.get("/check/:id", checkAuth, (req, res, next) => {
  FeedbackRequest.check({ oldInterviewId: req.params.id }, (err, doc) => {
    if (err) {
      res.status(500).json({
        error: err
      });
    } else if (doc.length)
      res.status(200).json({ doc: doc[0], message: "found" });
    else res.status(200).json({ message: "not found" });
  });
});
module.exports = router;
