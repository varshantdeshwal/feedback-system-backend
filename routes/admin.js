const express = require("express");
const router = express.Router();
const Admin = require("../utils/admin");
const checkAuth = require("../auth/check-auth");
router.get("/", (req, res, next) => {
  Admin.fetch({}, (err, doc) => {
    if (err) {
      console.log(err);
      res.status(500).json({
        error: err
      });
    }
    res.status(200).json(doc);
  });
});
router.post("/", checkAuth, (req, res, next) => {
  Admin.add(req.body, (err, doc) => {
    if (err) {
      console.log(err);
      res.status(500).json({
        error: err
      });
    }
    res.status(200).json(doc);
  });
});
router.delete("/:email", checkAuth, (req, res, next) => {
  User.delete({ email: req.params.email }, (err, doc) => {
    if (err) {
      throw err;
    }
    res.json({ message: "deleted", user: doc });
  });
});

module.exports = router;
