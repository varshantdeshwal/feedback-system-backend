var mongoose = require("mongoose");

const feedbackRequestSchema = mongoose.Schema({
  oldInterviewId: { type: String, required: true },
  newInterviewerId: { type: String, required: true },
  assignedBy: { type: String, required: true },
  applicantName: { type: String, required: true }
});

module.exports = mongoose.model("feedback-request", feedbackRequestSchema);
